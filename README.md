# FAST R WEB #

This is our version of FastRWeb to add additional capabilities like support for authToken to start with.

### This repository is for  ###

* Building R Web APIs
* Version 1.1-2

### Distribution ###
Point to your docker file references to the package in the dist folder.

### Contributions ###
use command to the pwd
```R
sudo R CMD build .
```
then copy the tar.gz file to public downloads folder in the repository. 

Run the docker build for updating the base image depending on this library.

In case of version change the dependent files SHOULD be updated, as any requests from components using non-latest builds of this software would not be entertained.

E.g.
LATEST version file name -> FastRWeb_1.1-2.tar.gz


### Who do I talk to? ###

* Satya Srikant Mantha